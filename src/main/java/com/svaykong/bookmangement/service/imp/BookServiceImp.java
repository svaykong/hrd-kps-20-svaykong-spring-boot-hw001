package com.svaykong.bookmangement.service.imp;

import com.svaykong.bookmangement.repository.BookRepository;
import com.svaykong.bookmangement.repository.dto.BookDto;
import com.svaykong.bookmangement.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insert(BookDto book) {
        boolean isInserted = bookRepository.insert(book);
        if(isInserted) {
            return book;
        }
        else
            return null;
    }

    @Override
    public List<BookDto> select() {
        return bookRepository.select();
    }
}
