package com.svaykong.bookmangement.service;

import com.svaykong.bookmangement.repository.dto.BookDto;

import java.util.List;

public interface BookService {

    BookDto insert(BookDto book);
    List<BookDto> select();

}
