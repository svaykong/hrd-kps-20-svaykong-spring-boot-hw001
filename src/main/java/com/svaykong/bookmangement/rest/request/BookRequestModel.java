package com.svaykong.bookmangement.rest.request;

public class BookRequestModel {

    private String title;
    private String author;
    private String description;
    private String thumbnail;
    private int category_id;

    public BookRequestModel() {}

    public BookRequestModel(String title, String author, String description, String thumbnail, int category_id) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "BookRequestModel{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", category_id=" + category_id +
                '}';
    }
}
