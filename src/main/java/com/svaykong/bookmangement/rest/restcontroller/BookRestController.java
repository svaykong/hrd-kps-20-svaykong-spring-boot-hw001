package com.svaykong.bookmangement.rest.restcontroller;

import com.svaykong.bookmangement.repository.dto.BookDto;
import com.svaykong.bookmangement.rest.request.BookRequestModel;
import com.svaykong.bookmangement.rest.response.BaseApiResponse;
import com.svaykong.bookmangement.service.imp.BookServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {

    private BookServiceImp bookServiceImp;

    @Autowired
    public void setBookServiceImp(BookServiceImp bookServiceImp) {
        this.bookServiceImp = bookServiceImp;
    }

    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(
            @RequestBody BookRequestModel article) {

        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        //TODO: Validate ->

        ModelMapper mapper = new ModelMapper();

        BookDto bookDto = mapper.map(article, BookDto.class);

        BookDto result = bookServiceImp.insert(bookDto);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);

        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BookRequestModel>> response =
                new BaseApiResponse<>();

        List<BookDto> articleDtoList = bookServiceImp.select();
        List<BookRequestModel> books = new ArrayList<>();

        for (BookDto articleDto : articleDtoList) {
            books.add(mapper.map(articleDto, BookRequestModel.class));
        }

        response.setMessage("You have found all books successfully");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

}
