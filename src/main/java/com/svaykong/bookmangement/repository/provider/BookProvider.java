package com.svaykong.bookmangement.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    /*public String selectByFilter(String articleId, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("articles");

            // Check condition
            if (articleId != null || !articleId.equals(""))
                WHERE("articleId = #{articleId}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }*/

}
