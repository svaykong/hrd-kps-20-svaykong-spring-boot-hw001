package com.svaykong.bookmangement.repository;

import com.svaykong.bookmangement.repository.dto.BookDto;
import com.svaykong.bookmangement.repository.provider.BookProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Insert("INSERT INTO tb_books (title, author, description, thumbnail, category_id) " +
            "VALUES (#{title}, #{author}, #{description}, #{thumbnail}, category_id)")
    boolean insert(BookDto book);

    // Using provider
    @SelectProvider(value = BookProvider.class, method = "select")
    List<BookDto> select();


}
